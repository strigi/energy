import { fetch_gas_indices } from '../backend/src/engie-scraper.mjs'
import {Indexation} from "../backend/src/indexation.model.mjs";
import logger from "../backend/src/logger.mjs";
import db from "../backend/src/database.mjs";
import {DateTime} from "luxon";

const log = logger('importer')

try {
    log.info('Scraping indexation parameters for natural gas from Engie website')
    const indexations = await fetch_gas_indices();
    await loadIntoDatabase(indexations)
    log.info('Import finished')
} catch(error) {
    log.error('Error while importing indexations', error)
} finally {
    await db.close();
}

async function loadIntoDatabase(indexations) {
    log.info('Loading %d indexation parameters into MongoDB', indexations.length)
    await db.connect()

    for(const indexation of indexations) {
        let document = await Indexation.findOne({date: indexation.date})
        let action;
        if(!document) {
            document = new Indexation(indexation)
            action = 'create'
            log.debug('Saving new indexation %j', document)
        } else {
            log.debug('Updating existing indexation %j', document)
            Object.assign(document, indexation);
            action = 'update'
        }
        await document.save();
        log.verbose('Imported indexation for month %s (%s)', DateTime.fromJSDate(document.date).toFormat('yyyy-MM'), action)
    }
}
