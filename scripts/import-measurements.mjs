import { readFile } from 'node:fs/promises'
import {Measurement} from '../backend/src/measurement.mjs'
import db from '../backend/src/database.mjs'
import logger from '../backend/src/logger.mjs'

const log = logger('importer')

try {
    await db.connect()
    const objects = await extractObjectsFromCSVFile('./steenbakkerijen.csv');
    const models = validateAndTransformToModels(objects)
    await loadModelsIntoMongoDB(models)
} finally {
    await db.close()
}

async function loadModelsIntoMongoDB(models) {
    await Measurement.deleteMany({})
    for(const model of models) {
        await model.save()
    }
}

function validateAndTransformToModels(objects) {
    const models = [];
    for (let i = 0; i < objects.length; i++) {
        const object = objects[i];
        log.info(`Validating and transforming object #${i} with timestamp '${object.timestamp}'`)
        const current = new Measurement(object);
        current.validateSync()

        if (i > 0) {
            for (const key of ['electricity_high', 'electricity_low', 'natural_gas', 'water']) {
                if(current[key] === null || current[key] === undefined) {
                    continue
                }
                let j = i - 1
                let previous = models[j];
                while(j >= 0 && (previous[key] === null || previous[key] === undefined)) {
                    j -= 1
                    previous = objects[j]
                }
                if (previous && previous[key] > current[key]) {
                    throw new Error(`Cumulative values must always be >= to the previous but values for rows #${i - 1} and #${i} do not satisfy this requirement because field '${key}' '${current[key]}' < '${previous[key]}'`)
                }
            }
        }

        models.push(current)
    }
    return models;
}

async function extractObjectsFromCSVFile(file_name) {
    const file = await readFile(file_name)
    const lines = file.toString().split('\n')

    // First line contains the field names
    const header = lines.shift();
    const columns = header.split(',')

    // Subsequent lines contain rows
    const objects = []
    for(let row_index = 0; row_index < lines.length; row_index++) {
        const line = lines[row_index];
        if(line === '') {
            continue
        }

        const cells = line.split(',')
        if(cells.length !== columns.length) {
            throw new Error(`Row ${row_index} does not contain equal number of cells in header. Expected ${columns.length} but was ${cells.length}: ${cells}`);
        }

        const measurement = {}
        for(let i = 0; i < columns.length; i++) {
            measurement[columns[i]] = cells[i]
        }
        objects.push(measurement)
    }
    return objects;
}
