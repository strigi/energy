export default {
    verbose: true,
    testMatch: ['**/*.spec.mjs'],
    transform: {},
    moduleNameMapper: {
        '#(.*)': '<rootDir>/node_modules/$1'
    },
    // restoreMocks: true,
}
