import { describe, test, expect, jest } from '@jest/globals'
import { LEVEL, MESSAGE } from 'triple-beam'
import logger from './logger.mjs'
import {DateTime} from "luxon";

describe('logger', () => {
    let spy;
    let log;

    beforeEach(() => {
        log = logger('rambo');
        spy = spyOnLogger(log)
    });

    test('log with simple message without splat is logged correctly', () => {
        log.info('Why Are You Pushing Me?')
        expect(spy.info).toEqual({
            [LEVEL]: 'info',
            level: 'info',
            label: 'rambo',
            message: 'Why Are You Pushing Me?',
            version: 'todo',
            timestamp: expect.any(DateTime)
        })
    })

    test('log with splat that is fully consumed is logged correctly', () => {
        log.debug('I Will %s To Keep Their %s Alive Forever.', 'Fight', 'Memory')
        expect(spy.info).toEqual({
            [LEVEL]: 'debug',
            level: 'debug',
            label: 'rambo',
            message: 'I Will Fight To Keep Their Memory Alive Forever.',
            version: 'todo',
            timestamp: expect.any(DateTime)
        })
    })

    test('log with splat that is longer than the number of placeholders adds unconsumed arguments as meta', () => {
        log.silly(`You're Not Hunting %s. He's Hunting %s.`, 'Him', 'You', 'Killer', {weapon: 'knife'}, new Error('death'), 911)
        expect(spy.info).toEqual({
            [LEVEL]: 'silly',
            level: 'silly',
            label: 'rambo',
            message: `You're Not Hunting Him. He's Hunting You.` ,
            version: 'todo',
            meta: [
                'Killer',
                { weapon: 'knife' },
                expect.objectContaining({
                    message: 'death'
                }),
                911
            ],
            timestamp: expect.any(DateTime)
        })
    })

    test('log with splat that is longer than the number of placeholders adds unconsumed arguments as meta', () => {
        log.error(`When You're %s, %s Is As Easy As %s`, 'Pushed', 'Killing', 'Breathing', 'Fighting', {weapon: 'knife'}, new Error('death'), 911)
        expect(spy.info).toEqual({
            [LEVEL]: 'error',
            level: 'error',
            label: 'rambo',
            message: `When You're Pushed, Killing Is As Easy As Breathing` ,
            version: 'todo',
            meta: [
                'Fighting',
                { weapon: 'knife' },
                expect.objectContaining({
                    message: 'death'
                }),
                911
            ],
            timestamp: expect.any(DateTime)
        })
    })

    test('log with consumed error does not add the error as meta', () => {
        const error = new Error('Become');
        error.toString = function() {
            return this.message
        }

        log.warn(`To %s A War, You Gotta %s War.`, 'Survive', error)
        expect(spy.info).toEqual({
            [LEVEL]: 'warn',
            level: 'warn',
            label: 'rambo',
            message: expect.stringContaining('To Survive A War, You Gotta Become War.'),
            version: 'todo',
            timestamp: expect.any(DateTime)
        })
    })

    test('log with unconsumed error adds the error as meta', () => {
        log.info(`Live For %s Or Die For %s`, 'Nothing', 'Something', new Error('death'))
        expect(spy.info).toEqual({
            [LEVEL]: 'info',
            level: 'info',
            label: 'rambo',
            message: 'Live For Nothing Or Die For Something',
            version: 'todo',
            timestamp: expect.any(DateTime),
            meta: [
                expect.any(Error)
            ]
        })
    })

    test('meta can be combined to be anything', () => {
        log.silly(`I'll Give You A War You Won't Believe`, new Error('first'), {second: 2}, 'third', new Error('fourth'), 5)
        expect(spy.info.meta).toEqual([
            new Error('first'),
            {second: 2},
            'third',
            new Error('fourth'),
            5
        ]);
    })

    test('logging an error solo uses error.message as info.message and adds error as meta', () => {
        log.error(new Error(`There's No Rescue Team. There's Just Me`))
        expect(spy.info).toEqual({
            [LEVEL]: 'error',
            level: 'error',
            label: 'rambo',
            message: `There's No Rescue Team. There's Just Me`,
            version: 'todo',
            meta: [
                new Error(`There's No Rescue Team. There's Just Me`)
            ],
            timestamp: expect.any(DateTime),
        })
    })

    test('logging an error solo removes winston pollution from the error object added as meta', () => {
        log.error(new Error(`I'm Gonna Tear You Apart.`))
        const error = spy.info.meta[0];
        expect(error).not.toHaveProperty('label')
        expect(error).not.toHaveProperty('level')
        expect(error.hasOwnProperty(LEVEL)).toBe(false)
        expect(error).not.toHaveProperty('version')
    })

    test('logging an object solo uses toString as message and adds object as meta', () => {
        const rambo = {
            first_name: 'John',
            last_name: 'Rambo',

            toString() {
                return `${this.first_name} ${this.last_name}`
            }
        }
        log.silly(rambo)

        expect(spy.info).toEqual({
            [LEVEL]: 'silly',
            level: 'silly',
            label: 'rambo',
            message: `John Rambo`,
            version: 'todo',
            meta: [ rambo ],
            timestamp: expect.any(DateTime),
        })
    })

    test('logging an object directly with extra meta combines the results', () => {
        const q = {
            question: 'Whatever Possessed God In Heaven To Make A Man Like Rambo?',
            difficulty: 5,

            toString() {
                return this.question
            }
        }
        const a = {
            answer: `I don't know`,
        };
        log.silly(q, a);

        expect(spy.info).toEqual({
            [LEVEL]: 'silly',
            level: 'silly',
            label: 'rambo',
            message: `Whatever Possessed God In Heaven To Make A Man Like Rambo?`,
            version: 'todo',
            meta: [ q, a ],
            timestamp: expect.any(DateTime),
        })
    })

    test.todo('console outputs for all tests')
    test.todo('version')
    test.todo('configure logged levels')
    test.todo('configure logged categories per level')

    /**
     * Sets a spy network to be able to intercept Winston info objects and the final output written to the console.
     * @param logger A Winston logger you wish to intercept.
     * @return {info: Object, console: String} An object with properties that reflect the values of the last call to the logger.
     */
    function spyOnLogger(logger) {
        const info_object_spy = jest.spyOn(logger.format, 'transform');

        // Assumes the first transport is the console transport
        const console_spy = jest.spyOn(logger.transports[0].format, 'transform');

        return {
            /**
             * The last call's info object. That is the object that is internally produced before being handed to any particular transport for serialization.
             * All transports share this same info object before they decide what to do with it.
             * @return {Object}
             */
            get info() {
                const results = info_object_spy.mock.results
                return results[results.length - 1].value
            },

            /**
             * The last call's final output written to the console. That is, the value after it has been serialized from the info object representation.
             * This value is specific only to the console output. It's assumed that the console logger is present as the first transport.
             */
            get console() {
                const results = console_spy.mock.results
                return results[results.length - 1].value[MESSAGE]
            }
        }
    }
})

