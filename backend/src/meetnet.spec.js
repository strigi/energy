const { test, expect } = require('@playwright/test');

class PageObject {
  #page;

  constructor(page) {
    this.#page = page;    
  }

  async open() {
    await this.#page.goto('https://meetnet-mobile.dev.imdcapps.be')
    return this
  }

  async login(username, password) {
    await this.#page.locator('input[name="username"]').fill(username)  
    await this.#page.locator('input[name="password"]').fill(password)
    await this.#page.locator('button[type="submit"]').click()
    return this
  }

  async assertSitesShown(expectedSites) {
    const rows = await this.#page.locator('tr');
    await expect(rows).toHaveCount(expectedSites.length)
    let i = 0;
    for(const row of await rows.all()) {
      const cell = await row.locator('td > div')
      expect(await cell.first().innerText()).toBe(expectedSites[i].name)
      expect(await cell.last().innerText()).toBe(expectedSites[i].address)
      i += 1
    }
    return this;
  }
}

// Run tests in this file with portrait-like viewport.
test.use({
  viewport: {
    width: 600,
    height: 900
  }
});

test('my portrait test', async ({ page }) => {
  const home = new PageObject(page);

  await (await home.open()).login('kvr', 'tiYsCpw4_now');

  await home.assertSitesShown([
    { name: 'Molenbeek Lierde', address: '(geen adres)' },
    { name: 'Beerhofbeek Nazareth', address: 'Deinzestraat 39, Nazareth' },
    { name: 'Maarkebeek Maarkedal', address: 'Zottegemstraat Maarkedal' },
    { name: 'Molenbeek Ninove', address: 'Smid Lambrechtstraat 30, Ninove' },
    { name: 'Ophasseltbeek Lierde', address: 'Struntel. Lierde' },
    { name: 'Torensbeek Aalst', address: 'Vijverstraat 2. Aalst' }
  ])
});
