import mongoose from 'mongoose'

const schema = new mongoose.Schema({
    date: {
        type: Date,
        required: true,
        unique: true,
    },

    /**
     * Official price indexation parameter for natural gas contracts (e.g. 'variable' aka 'indexed' contracts) that allow their price to be changed every three months.
     * Represents the monthly arithmetic mean of "settlement price" of the quotations of "Dutch TTF Gas Base Load Futures" for the month preceding the trimester of delivery
     * (whatever that's supposed to means).
     */
    ttf_103_heren: {
        type: Number,
        required: true,
    },

    ttf_101_heren: {
        type: Number,
        required: false,
    },

    zig_daq: {
        type: Number,
        required: false,
    },

    zig_dam: {
        type: Number,
        required: false,
    },
});

export const Indexation = mongoose.model('indexation', schema)
