/**
 * https://www.engie.be/nl/energie/elektriciteit-gas/prijzen-voorwaarden
 * # EASY Indexed (aka EASY Variabel)
 * - prijs wordt aangepast (e.g. geïndexeerd) aan de huidige energieprijzen (groothandelmarkt ESGM (TTF) Nederland) PER 3 MAANDEN (trimester === kwartaal === 3 maanden === 1/4 van een jaar)
 * - TTF prijzen staan gepubliceerd op website Engie + CREG en zijn een gemiddelde van de dagprijzen van de maand voorafgaand aan de trimester van de levering.
 * Voorbeeld: contract start op 1 februari 2022. Prijs ligt dan vast voor de periodes:
 *  - FEB, MAART, APRIL (prijs A)
 *  - MEI, JUNI, JULI (prijs B)
 *  - AUGUSTUS, SEPTEMBER, OKTOBER (prijs C)
 *  - NOVEMBER, DECEMBER, JANUARI (prijs D)
 *  Ik denk dat de aanpassing van de prijs gemaakt wordt op basis van de op dat moment geldende TTF103 en die prijs geldt gedurende de volgende 3 maanden
 * - Momenteel 6% BTW (2022-12-21)
 * - Allerlei taksen en accijnzen en distributiekosten bijtellen (ongeveer 20% van de totaalprijs is mijn inschatting op basis van piechart guestimate)
 * Formule excl. BTW (december 2022) = 1.8066 + 0.1 * TTF103 (wat is dit voor mijn contract, wanneer is mijn contract gestart?)
 * Mijn contract begint op 2022-10-14
 * TTF103 voor December 2022 = 212.3097 €/MWhr
 * ## Elektriciteit
 * (todo)
 * ## Gas
 * Vaste vergoeding: 40.81€
 * Prijs per kWh: 0.24420€ (kwartaalprijzen)
 * Prijs per kWh: 0.17253 (geschatte jaarprijzen)
 */

import {chromium} from 'playwright'
import logger from "./logger.mjs";

const log = logger('scraper')

export async function fetch_gas_indices() {
    return await extractIndexObjects('https://www.engie.be/nl/energie/elektriciteit-gas/prijzen-voorwaarden/indexatieparameters/indexatieparameters-gas', row => {
        return {
            date: row[0],
            ttf_103_heren: row[1],
            zig_daq: row[2],
            zig_dam: row[3],
            ttf_101_heren: row[4],
        }
    })
}

export async function fetch_electricity_indices() {
    return await extractIndexObjects('https://www.engie.be/nl/energie/elektriciteit-gas/prijzen-voorwaarden/indexatieparameters/indexatieparameters-elektriciteit', row => {
        return {
            date: row[0],
            endex_303: row[1],
            endex_103: row[2],
            epex_dam: row[3],
        }
    })
}

async function extractIndexObjects(url, row_mapper) {
    const table = await extractIndexTable(url)
    const objects = [];
    for(const row of table) {
        const date = parseDate(row[0]);
        if(date === null) {
            continue
        }
        row[0] = date;
        for(let i = 1; i < row.length; i += 1) {
            row[i] = parseNumber(row[i])
        }
        objects.push(row_mapper(row))
    }
    return objects
}

async function extractIndexTable(url) {
    let browser;
    try {
        browser = await chromium.launch({
            headless: true,
        });

        log.info(`Scraping indices from '${url}'`)

        const page = await browser.newPage()
        await page.goto(url)
        // await page.waitForLoadState()
        log.debug('Page loaded')

        // Accept cookies
        await page.locator('button').getByText('Alles accepteren').click()

        const rowLocator = page.locator('.table_body').first().locator('.table_row');
        const rowCount = await rowLocator.count();
        const records = []
        for (let i = 0; i < rowCount; i += 1) {
            const row = rowLocator.nth(i);
            const cellLocator = row.locator('.table_cell')
            const cellCount = await cellLocator.count()
            if(cellCount === 0) {
                continue
            }

            const record = [];
            for(let j = 0; j < cellCount; j += 1) {
                record.push(await cellLocator.nth(j).innerText())
            }
            records.push(record);
        }
        log.debug(`Scraped ${records.length} records`)
        return records;
    } finally {
        if(browser) {
            await browser.close()
        }
    }
}

function parseNumber(number_string) {
    const value = parseFloat(number_string.replaceAll(',', '.'))
    if(isNaN(value)) {
        return null
    }

    return value;
}

function parseDate(date_string) {
    const match = /^([A-Za-z]+) (202\d)$/.exec(date_string)
    if(!match) {
        return null;
    }

    const months = ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'October', 'November', 'December']
    const month = months.indexOf(match[1]);
    if(month === -1) {
        return null;
    }

    const year = parseInt(match[2]);

    return new Date(Date.UTC(year, month, 1))
}