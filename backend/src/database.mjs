import mongoose from 'mongoose';
import logger from './logger.mjs'

const log = logger('db')
const query_log = logger('db.query')

const uri = 'mongodb://mars.eothel.be:27017/energy'

export const db = {
    connect,
    close
}

export default db;

async function connect() {
    log.verbose(`Connecting to database '%s'`, uri)
    mongoose.set('debug', debugLoggerFn)
    mongoose.set('strictQuery', false)
    await mongoose.connect(uri);
}

async function close() {
    if(mongoose.connection.readyState === 1) {
        log.verbose(`Closing database connection to '%s'`, uri)
        await mongoose.connection.close();
    }
}

/**
 * Makes Mongoose use our app's logging infrastructure when debug is set to `true`
 */
function debugLoggerFn(collection, method, filter, options) {
    query_log.silly(`Executing MongoDB query: %s.%s(%j, %j)`, collection, method, filter, options)
}
