import util from 'node:util';
import path from 'node:path';
import {fileURLToPath} from 'node:url'
import {DateTime} from 'luxon'
import winston from 'winston'
import chalk from 'chalk';
import {LEVEL, SPLAT} from 'triple-beam';

/**
 * Configures the logger's enabled level and label. *
 * Keys are regex strings for matching against labels and values are the maximum log level
 * @example ```
 * // Log `error`, `warn` and `info` messages for all loggers with labels starting with `foo`
 * '^foo': 'info'
 * ```
 * @type {Object}
 */
const config = {
    '.*': 'silly'
};

process.on('uncaughtException', function(error, origin) {
    logger('process').error(`Uncaught exception from origin '%s'`, origin, error)
})

export function logger(id) {
    if(!winston.loggers.has(id)) {
        winston.loggers.add(id, createLogger(id));
    }
    return winston.loggers.get(id);
}

export default logger;

function createLogger(id) {
    return {
        handleRejections: true,
        handleExceptions: true,
        defaultMeta: {
            version: 'todo',
            label: id
        },
        format: winston.format.combine(
            filter(config),
            format()
        ),
        transports: [
            new winston.transports.Console({
                level: 'silly',
                format: winston.format.combine(
                    winston.format.printf(consolePrinter(10, 10))
                )
            }),
            createFileTransport('error'),
            createFileTransport('warn'),
            createFileTransport('info'),
            createFileTransport('verbose'),
            createFileTransport('debug'),
            createFileTransport('silly'),
        ]
    };
}

function createFileTransport(level) {
    const filename = path.join(path.dirname(fileURLToPath(import.meta.url)), `../../logs/${level}.log`)
    return new winston.transports.File({
        level: level,
        filename: filename,
        format: winston.format.json(),
    })
}

const filter = winston.format((info, opts) => {
    const rules = typeof opts === 'function' ? opts() : opts
    const { levels } = winston.loggers.get(info.level)
    for(const [key, threshold] of Object.entries(rules)) {
        if(new RegExp(key).test(info.label)) {
            if(levels[threshold] >= levels[info.level]) {
                return info
            } else {
                return null
            }
        }
    }

    return null;
})

const format = winston.format((old_info, _opts) => {
    const new_info = {}

    // Add level
    new_info[LEVEL] = old_info[LEVEL]
    new_info.level = old_info[LEVEL]

    // Add timestamp
    new_info.timestamp = DateTime.now()

    // Add logger's default static props (label, version, ...)
    const { defaultMeta } = winston.loggers.get(old_info.label);
    Object.assign(new_info, defaultMeta)

    // // Special case, logger called with object and without message e.g. `logger.info(object)`
    if(typeof old_info.message !== 'string') {
        const object = old_info.message;
        old_info.message = object.toString()
        new_info.meta = [object]
    }

    // Interpolate message string with splat arguments
    // const splat = old_info[SPLAT] ?? []
    const { args, meta } = separate_splat(old_info)
    new_info.message = util.format(old_info.message, ...args).trim()

    // Special case when passed an error, its fields are destructured because Winston considers it a meta object.
    if(old_info instanceof Error) {
        delete old_info[LEVEL]
        delete old_info.level
        Object.keys(defaultMeta).forEach(m => {
            delete old_info[m]
        })
        meta.push(old_info)
    }

    // Add the unused part of the splat arguments as meta
    if(meta.length > 0) {
        new_info.meta = [...(new_info.meta ?? []), ...meta]
    }

    return new_info

    function separate_splat(info) {
        const splat = info[SPLAT] ?? []
        const count = info.message.match(/%[sdifjoOc]/g)?.length ?? 0
        return {
            args: splat.slice(0, count),
            meta: splat.slice(count)
        }
    }
});

function consolePrinter(maximum_label_size, minimum_label_size, ellipsis = '~') {
    return info => {
        const aligned_level = alignLevel(info);
        const aligned_label = alignLabel(info.label)

        const formatted_timestamp = format(info.level, DateTime.fromISO(info.timestamp).toFormat('yyyy-MM-dd HH:mm:ss'), { variant: 'dim' })
        const formatted_level = format(info.level, aligned_level, { variant: 'bright', bold: true })
        const formatted_label = format(info.level, aligned_label, { italic: true })
        const formatted_message = format(info.level, info.message, { variant: 'bright' })
        const formatted_meta = format(info.level, append_meta(info), { variant: 'dim' });

        return `${formatted_timestamp} ${formatted_level} ${formatted_label} ${formatted_message}${formatted_meta}`
    }

    function append_meta(info) {
        if(!info.meta) {
            return ''
        }

        let output = ''
        for(const meta of info.meta) {
            output = `${output}\n${util.inspect(meta)}`
        }

        return output;
    }

    function alignLevel(info) {
        const logger = winston.loggers.get(info.label);
        const { levels } = logger

        const largest_level_length = Object.keys(levels).map(l => l.length).reduce((a, b) => Math.max(a, b), 0)
        return info[LEVEL].padEnd(largest_level_length);
    }

    function alignLabel(label) {
        const labels = [...winston.loggers.loggers.keys()]
        const truncated_labels = labels.map(label => truncate(label, maximum_label_size, ellipsis).padEnd(minimum_label_size))
        const largest_truncated_label_length = truncated_labels.reduce((a, b) => Math.max(a, b.length), 0)
        return truncate(label, maximum_label_size, ellipsis).padEnd(largest_truncated_label_length)
    }

    function truncate(value, limit, ellipsis) {
        const maximum_length = limit - ellipsis.length;
        if(value.length <= maximum_length) {
            return value;
        }
        return `${value.substring(0, maximum_length)}${ellipsis}`;
    }

    function format(level, value, options) {
        if(value === null || value === undefined ||  value.trim() === '') {
            return ''
        }

        const default_options = {
            variant: 'normal',
            bold: false,
            italic: false,
            underline: false,
        }
        const merged_options = {...default_options, ...options};

        const color_map = {
            silly: {
                dim: v => chalk.dim(chalk.magenta(v)),
                normal: chalk.magenta,
                bright: chalk.magentaBright,
            },
            debug: {
                dim: v => chalk.dim(chalk.blue(v)),
                normal: chalk.blue,
                bright: chalk.blueBright,
            },
            info: {
                dim: v => chalk.dim(chalk.green(v)),
                normal: chalk.green,
                bright: chalk.greenBright,
            },
            warn: {
                dim: v => chalk.dim(chalk.yellow(v)),
                normal: chalk.yellow,
                bright: chalk.yellowBright,
            },
            error: {
                dim: v => chalk.dim(chalk.red(v)),
                normal: chalk.red,
                bright: chalk.redBright
            },

            verbose: {
                dim: v => chalk.dim(chalk.cyan(v)),
                normal: chalk.cyan,
                bright: chalk.cyanBright
            },

            /**
             * Used when a level is used that is not in the map.
             */
            fallback: {
                dim: v => chalk.dim(chalk.white(v)),
                normal: chalk.white,
                bright: chalk.whiteBright
            }
        }

        const color_pack = color_map[level] ?? color_map['fallback']
        const colorFn = color_pack[merged_options.variant];

        let formatted_value = colorFn(value)

        if(merged_options.bold) {
            formatted_value = chalk.bold(formatted_value)
        }

        if(merged_options.italic) {
            formatted_value = chalk.italic(formatted_value)
        }

        if(merged_options.underline) {
            formatted_value = chalk.underline(formatted_value)
        }

        return formatted_value
    }
}
