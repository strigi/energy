import Router from '@koa/router'
import {Measurement} from "./measurement.mjs";
import {Indexation} from "./indexation.model.mjs";

const router = new Router()
export default router;

router.get('/measurements', async ctx => {
    ctx.body = await Measurement.find().sort({ timestamp: 'ascending' });
})

router.get('/indexations', async ctx => {
    ctx.body = await Indexation.find().sort({ date: 'ascending' });
})
