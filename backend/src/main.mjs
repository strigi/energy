import db from './database.mjs'
import logging from './logger.mjs'
import Koa from 'koa'
import api from "./api.mjs";

const logger = logging('app')

logger.info('Starting app')
const app = new Koa()
app.use(api.prefix('/api').routes())

logger.verbose('Registering SIGINT listener')
process.on('SIGINT', async (signal, code) => {
    logger.warn('Received signal %s %s. Shutting down.', signal, code)
    await db.close();
    process.exit();
});

await db.connect();
const port = 1507;
app.listen(port, () => {
    logger.info('Webserver ready and Listening on port %d', port)
});