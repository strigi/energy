// // parameters:
//     * TTF103 referentie
//     * Formule Engie
//     * BTW 6% of 21%
//     * Netwerkkosten (tabel per netbeheerder en per verbruiksklasse klein, gemiddeld, groot, meteropname & transport)
//     * Vaste vergoeding: 40.81 excl.
//
// conversie m³ naar kWh? zie factuur? of ergens Online??? (CREG, VREG?)

import mongoose from 'mongoose'

const schema = new mongoose.Schema({
    timestamp: {
        type: Date,
        required: true,
    },

    electricity_low: {
        type: Number,
        required: false,
    },

    electricity_high: {
        type: Number,
        required: false,
    },

    natural_gas: {
        type: Number,
        required: false,
    },

    water: {
        type: Number,
        required: false,
    },

    comment: {
        type: String,
        required: false,
        trim: true,
    }
})

export const Measurement = mongoose.model('measurement', schema)
