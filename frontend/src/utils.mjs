export class Point {
    static fromArray(couple) {
        return new Point(couple[0], couple[1])
    }

    static fromObject(couple) {
        return new Point(couple.x, couple.y)
    }

    constructor(x, y) {
        this.x = this[0] = x
        this.y = this[1] = y
    }
}

export class Domain {
    static fromArray(couple) {
        return new Domain(couple[0], couple[1])
    }

    static fromObject(couple) {
        return new Domain(couple.from, couple.to)
    }

    constructor(from, to) {
        this.from = this[0] = from
        this.to = this[1] = to
    }

    contains(value) {
        return this.from <= value && this.to >= value
    }

    expand(value)  {
        if(this.from > value) {
            this.from = value
        }

        if(this.to < value) {
            this.to = value
        }
    }
}