import {DateTime} from "luxon";

export async function fetchMeasurements() {
    const response = await fetch('/api/measurements')
    const data = await response.json();
    return data.map(d => {
        return { ...d, timestamp: DateTime.fromISO(d.timestamp) }
    })
}

export async function fetchIndexations() {
    const response = await fetch('/api/indexations')
    const data = await response.json();
    return data.map(d => {
        return { ...d, date: DateTime.fromISO(d.date) }
    })
}

export async function fetchIndexationByTimestamp(timestamp) {
    const indexations = await fetchIndexations();

    if(indexations.length === 0) {
        return null
    }

    let previous = indexations[0];
    for (const current of indexations) {
        if(current.date >= timestamp) {
            break
        }
        previous = current
    }
    return previous
}